import { Model } from "sequelize-typescript";
export declare class CartProduct extends Model<CartProduct> {
    cartId: number;
    productId: number;
}
