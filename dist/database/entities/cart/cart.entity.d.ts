import { Model } from "sequelize-typescript";
import { Product } from "../product/product.entity";
export declare class Cart extends Model<Cart> {
    orderId: number;
    products: Product[];
}
