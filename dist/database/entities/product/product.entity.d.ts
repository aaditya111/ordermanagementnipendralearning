import { Model } from "sequelize-typescript";
export declare class Product extends Model<Product> {
    name: string;
    description: string;
    price: string;
    quantity: number;
}
