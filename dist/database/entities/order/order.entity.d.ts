import { Model } from "sequelize-typescript";
import { Cart } from "../cart/cart.entity";
export declare class Order extends Model<Order> {
    firstName: string;
    lastName: string;
    email: string;
    address: string;
    city: string;
    state: string;
    zipCode: string;
    creditCardNumber: string;
    expirationMonth: string;
    expirationYear: string;
    cvv: string;
    carts: Cart[];
}
