import { CreateCartDto } from "./dtos/create-cart.dto";
import { Cart } from "src/database/entities/cart/cart.entity";
import { AddToCartDto } from "./dtos/update-cart.dto";
import { RemoveFromCartDto } from "./dtos/remove-from-cart.dto";
export declare class CartService {
    private cartModel;
    constructor(cartModel: typeof Cart);
    findByOrderId(orderId: number): Promise<Cart>;
    create(cartDto: CreateCartDto): Promise<Cart>;
    addToCart(cartId: number, addToCartDto: AddToCartDto): Promise<Cart>;
    removeFromCart(cartId: number, removeFromCartDto: RemoveFromCartDto): Promise<Cart>;
}
