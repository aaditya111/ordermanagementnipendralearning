import { RemoveFromCartDto } from "./dtos/remove-from-cart.dto";
import { CartService } from "./cart.service";
import { CreateCartDto } from "./dtos/create-cart.dto";
import { Cart } from "src/database/entities/cart/cart.entity";
import { AddToCartDto } from "./dtos/update-cart.dto";
export declare class CartController {
    private cartService;
    constructor(cartService: CartService);
    create(cartDto: CreateCartDto): Promise<Cart>;
    addToCart(addToCartDto: AddToCartDto, cartId: number): Promise<Cart>;
    removeFromCartDto(removeFromCartDto: RemoveFromCartDto, cartId: number): Promise<Cart>;
}
