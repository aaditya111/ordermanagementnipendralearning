import { Product } from "../../database/entities/product/product.entity";
import { CreateProductDto } from "./dtos/create-product.dto";
import { UpdateProductDto } from "./dtos/update-product.dto";
import { ProductService } from "src/modules/product/product.service";
export declare class ProductController {
    private productService;
    constructor(productService: ProductService);
    findAll(page?: number, limit?: number): Promise<Product[]>;
    findById(id: number): Promise<Product>;
    create(productDto: CreateProductDto): Promise<Product>;
    update(id: number, productDto: UpdateProductDto): Promise<Product>;
    delete(id: number): Promise<void>;
}
