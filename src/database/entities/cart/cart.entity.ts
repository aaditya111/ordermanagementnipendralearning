import {
  Table,
  Column,
  Model,
  BelongsToMany,
  ForeignKey,
} from "sequelize-typescript";
import { Order } from "../order/order.entity";
import { Product } from "../product/product.entity";
import { CartProduct } from "./cart-product.entity";

@Table
export class Cart extends Model<Cart> {
  @ForeignKey(() => Order)
  @Column
  orderId: number;

  @BelongsToMany(() => Product, () => CartProduct)
  products: Product[];
}
