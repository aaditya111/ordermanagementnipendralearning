import { ForeignKey, Table, Model, Column } from "sequelize-typescript";
import { Cart } from "./cart.entity";
import { Product } from "../product/product.entity";

@Table
export class CartProduct extends Model<CartProduct> {
  @ForeignKey(() => Cart)
  @Column
  cartId: number;

  @ForeignKey(() => Product)
  @Column
  productId: number;
}
