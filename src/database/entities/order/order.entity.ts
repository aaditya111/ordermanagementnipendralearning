import { Table, Column, Model, HasMany } from "sequelize-typescript";
import { Cart } from "../cart/cart.entity";

@Table
export class Order extends Model<Order> {
  @Column
  firstName: string;

  @Column
  lastName: string;

  @Column
  email: string;

  @Column
  address: string;

  @Column
  city: string;

  @Column
  state: string;

  @Column
  zipCode: string;

  @Column
  creditCardNumber: string;

  @Column
  expirationMonth: string;

  @Column
  expirationYear: string;

  @Column
  cvv: string;

  @HasMany(() => Cart)
  carts: Cart[];
}
