import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from "@nestjs/common";
import { InjectModel } from "@nestjs/sequelize";
import { Product } from "../../database/entities/product/product.entity";
import { CreateProductDto } from "./dtos/create-product.dto";
import { UpdateProductDto } from "./dtos/update-product.dto";

@Injectable()
export class ProductService {
  constructor(
    @InjectModel(Product)
    private productModel: typeof Product,
  ) {
    console.log("Product Service Triggered");
  }

  async findAll(options: { page: number; limit: number }): Promise<Product[]> {
    try {
      const { page, limit } = options;
      const offset = (page - 1) * limit;
      const products = await this.productModel.findAll({ limit, offset });
      return products;
    } catch (err) {
      throw new InternalServerErrorException(err.message);
    }
  }

  async findById(id: number): Promise<Product> {
    const product = await this.productModel.findByPk(id);
    if (!product) {
      throw new NotFoundException(`Product with ID ${id} not found`);
    }
    try {
      return product;
    } catch (err) {
      console.log(err);
    }
  }

  async create(productDto: CreateProductDto): Promise<Product> {
    return this.productModel.create(productDto);
  }

  async update(id: number, productDto: UpdateProductDto): Promise<Product> {
    const { name, description, price, quantity } = productDto;
    if (name || description || price || quantity) {
      try {
        const product = await this.productModel.findByPk(id);
        return product.update(productDto);
      } catch (err) {
        throw new InternalServerErrorException(err.message);
      }
    } else {
      throw new BadRequestException();
    }
  }

  async delete(id: number): Promise<void> {
    const product = await this.productModel.findByPk(id);
    if (!product) {
      throw new NotFoundException(`Product with ID ${id} not found`);
    }
    try {
      await product.destroy();
    } catch (err) {
      throw new InternalServerErrorException(err.message);
    }
  }
}
