import { Product } from "../../database/entities/product/product.entity";
import { CreateProductDto } from "./dtos/create-product.dto";
import { UpdateProductDto } from "./dtos/update-product.dto";
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
} from "@nestjs/common";
import { ProductService } from "src/modules/product/product.service";

@Controller("products")
export class ProductController {
  constructor(private productService: ProductService) {
    console.log("Product Controller Triggered");
  }

  @Get()
  async findAll(
    @Query("page") page: number = 1,
    @Query("limit") limit: number = 10,
  ): Promise<Product[]> {
    const products = await this.productService.findAll({
      page: Number(page),
      limit: Number(limit),
    });
    return products;
  }

  @Get(":id")
  async findById(@Param("id") id: number): Promise<Product> {
    const product = await this.productService.findById(id);
    return product;
  }

  @Post()
  async create(@Body() productDto: CreateProductDto): Promise<Product> {
    return this.productService.create(productDto);
  }

  @Put(":id")
  async update(
    @Param("id") id: number,
    @Body() productDto: UpdateProductDto,
  ): Promise<Product> {
    const updatedProduct = await this.productService.update(id, productDto);
    return updatedProduct;
  }

  @Delete(":id")
  async delete(@Param("id") id: number): Promise<void> {
    const deletedProduct = await this.productService.delete(id);
    return deletedProduct;
  }
}
