import { Module } from "@nestjs/common";
import { SequelizeModule } from "@nestjs/sequelize";
import { CartService } from "./cart.service";
import { Cart } from "src/database/entities/cart/cart.entity";
import { CartController } from "./cart.controller";
import { CartProduct } from "src/database/entities/cart/cart-product.entity";

@Module({
  imports: [SequelizeModule.forFeature([Cart, CartProduct])],
  controllers: [CartController],
  providers: [CartService],
})
export class CartModule {}
