import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/sequelize";
import { CreateCartDto } from "./dtos/create-cart.dto";
import { Cart } from "src/database/entities/cart/cart.entity";
import { AddToCartDto } from "./dtos/update-cart.dto";
import { RemoveFromCartDto } from "./dtos/remove-from-cart.dto";

@Injectable()
export class CartService {
  constructor(
    @InjectModel(Cart)
    private cartModel: typeof Cart,
  ) {}

  async findByOrderId(orderId: number): Promise<Cart> {
    return this.cartModel.findOne({
      where: { orderId },
      include: { all: true },
    });
  }

  async create(cartDto: CreateCartDto): Promise<Cart> {
    return this.cartModel.create(cartDto);
  }

  async addToCart(cartId: number, addToCartDto: AddToCartDto): Promise<Cart> {
    const { productId } = addToCartDto;
    const cart = await this.cartModel.findByPk(cartId);
    await cart.$add("products", productId);
    return this.findByOrderId(cart.orderId);
  }

  async removeFromCart(
    cartId: number,
    removeFromCartDto: RemoveFromCartDto,
  ): Promise<Cart> {
    const { productId } = removeFromCartDto;
    const cart = await this.cartModel.findByPk(cartId);
    await cart.$remove("products", productId);
    return this.findByOrderId(cart.orderId);
  }
}
