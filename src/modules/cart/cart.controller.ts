import { RemoveFromCartDto } from "./dtos/remove-from-cart.dto";
import { Product } from "../../database/entities/product/product.entity";
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
} from "@nestjs/common";
import { CartService } from "./cart.service";
import { CreateCartDto } from "./dtos/create-cart.dto";
import { Cart } from "src/database/entities/cart/cart.entity";
import { AddToCartDto } from "./dtos/update-cart.dto";

@Controller("products")
export class CartController {
  constructor(private cartService: CartService) {
    console.log("Cart Controller Triggered");
  }

  @Post()
  async create(@Body() cartDto: CreateCartDto): Promise<Cart> {
    return this.cartService.create(cartDto);
  }

  @Post()
  async addToCart(
    @Body() addToCartDto: AddToCartDto,
    @Param("cartId") cartId: number,
  ): Promise<Cart> {
    return this.cartService.addToCart(cartId, addToCartDto);
  }

  @Post()
  async removeFromCartDto(
    @Body() removeFromCartDto: RemoveFromCartDto,
    @Param("cartId") cartId: number,
  ): Promise<Cart> {
    return this.cartService.removeFromCart(cartId, removeFromCartDto);
  }
}
